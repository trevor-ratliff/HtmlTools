//====
/// @file TimeBudgeter.js
/// @brief This file controls the application initialization and ui actions
/// @author Trevor Ratliff
/// @date 2022-11-21
//  
//  Definitions:
//	timeBudgeter - object holding main application logic
//	console - object to ensure logging can happen
//	common - variable access for the imported common code
//
/// @verbatim
/// History:  Date  |  Programmer  |  Contact  |  Description  |
///	2022-11-21  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  file 
///		creation  |
///	2023-02-06  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  started 
///		fleshing out init script  |
///	2023-04-15  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  working
///		out issuses with the sort
/// @endverbatim
//====
//import { common, sortItems } from "common";

var timeBudgeter = typeof timeBudgeter != 'undefined' ? timeBudgeter : 
	{
		'activityImportanceScale': 'hours',
		'activityImportanceValue': 4,
		'activityUrgencyScale': 'hours',
		'activityUrgencyValue': 1,
		'reminderImportanceScale': 'minutes',
		'reminderImportanceValue': 30,
		'reminderUrgencyScale': 'hours',
		'reminderUrgencyValue': 1,
		'testSchedules': false,
		'interval': 1,
		'noticeOn': true,
		'noticeInterval': 15,
		'logLevel': 6,		// 'trace' = 1, 'debug' = 2, 'warn' = 3, 'error' = 4, 'info' = 5
		'nextId': 1
	};

var console = console ?? 
	{ 
		"log": (data) => { alert(data); }, 
		"error": (data) => { alert(data); } 
	};


//----
// imprort sort function
//----
var common = {};
import("./common.js?v=0.3").then((module) => {
// Do something with the module.
	common = module;
	if (timeBudgeter.LogLevel <= 5) console.log('loaded common module', common);
});


//==--
// system methods
//==--

//----
/// @fn timeBudgeter.Init(options)
/// @brief Init gathers data and populates the various lists that drive the UI
/// @author Trevor Ratliff
/// @date 2022-11-21
/// @param options -- not used yet
/// @return
//----
timeBudgeter.Init = (options) => {
	let tb = timeBudgeter || {};
	console.log("Initialized TimeBudgeter");
	tb.data = [];

	// get querystring data
	tb.query = self.location.search.match(/([^?&=]+)/gi);
	tb.q = {};
	for (let ii = 0; ii < tb.query.length; ii+=2) {
		tb.q[tb.query[ii]] = tb.query[ii + 1];
	}

	// load data from storage
	if (!!localStorage) {
		// activity settings
		tb.activityImportanceScale = 
			localStorage.getItem('activityImportanceScale') || 
			tb.activityImportanceScale;

		tb.activityImportanceValue = 
			localStorage.getItem('activityImportanceValue') || 
			tb.activityImportanceValue;

		tb.activityUrgencyScale =
			localStorage.getItem('activityUrgencyScale') || 
			tb.activityUrgencyScale;

		tb.activityUrgencyValue =
			localStorage.getItem('activityUrgencyValue') || 
			tb.activityUrgencyValue;

		// reminder settings
		tb.reminderImportanceScale = 
			localStorage.getItem('reminderImportanceScale') || 
			tb.reminderImportanceScale;

		tb.reminderImportanceValue = 
			localStorage.getItem('reminderImportanceValue') || 
			tb.reminderImportanceValue;

		tb.reminderUrgencyScale =
			localStorage.getItem('reminderUrgencyScale') || 
			tb.reminderUrgencyScale;

		tb.reminderUrgencyValue =
			localStorage.getItem('reminderUrgencyValue') || 
			tb.reminderUrgencyValue;
		
		tb.testSchedules = 
			tb.q.testSchedules ||
			localStorage.getItem('testSchedules') ||
			tb.testSchedules;
		
		// interval setting
		tb.interval = 
			tb.q.interval ||
			localStorage.getItem('interval') ||
			tb.interval;

		// notice settings
		tb.noticeOn =
			localStorage.getItem('noticeOn') || 
			tb.noticeOn;

		tb.noticeInterval =
			tb.q.notice ||
			localStorage.getItem('noticeInterval') ||
			tb.noticeInterval;

		// set log level
		tb.logLevel =
			parseInt(tb.q.logLevel) ||
			parseInt(localStorage.getItem('logLevel')) ||
			tb.logLevel;
		if (isNaN(tb.logLevel)) tb.logLevel = 5;

		// set next id
		tb.nextId =
			localStorage.getItem('nextId') ||
			tb.nextId;

		// set item data
		tb.dataString = localStorage.getItem('TimeBudgeter');
		if(!!tb.dataString) {
			tb.data = JSON.parse(tb.dataString);
		} else {
			tb.dataString = '';
			localStorage.setItem('TimeBudgeter', tb.dataString);
		}
	}
}

//----
///	@fn ProcessData(data)
///	@brief adds the data to the UI creating elements as needed
///	@outhor Trevor Ratliff
///	@date 2023-02-07
///	@param data - the source data
///	@return
//----
timeBudgeter.ProcessData = (data) => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('processing data');
	if (tb.logLevel <= 2) console.log('data:', data);

	try {
		// clear grids
		tb.importantUrgent.replaceChildren([]);
		tb.importantNonurgent.replaceChildren([]);
		tb.unimportantUrgent.replaceChildren([]);
		tb.unimportantNonurgent.replaceChildren([]);

		// loop through data and add items to UI as appropriate
		for(let ii = 0; ii < data.length; ii++) {
			if (data[ii].done == true) continue;
			if (data[ii].type == 'reminder' && tb.testSchedules) {
				// test for a reminder instance - this will probably go in schedule.js
				continue;
			}
			let gridSection = tb.CalculateGridSection(data[ii]);
			let e = tb.itemTemplate.content.firstElementChild.cloneNode(true);
			e.id = data[ii].id;
			e.setAttribute('data-type', data[ii].type);
			e.setAttribute('data-due-on', data[ii].due);
			e.setAttribute('data-importance', data[ii].importance);
			e.setAttribute('data-urgence', data[ii].urgence);
			e.querySelector('.title').innerHTML = data[ii].title;
			e.querySelector('.note').innerHTML = data[ii].note;
			document.querySelector(`#${gridSection}`)
				.appendChild(e);
		}
	} catch (ex) {
		console.error(ex);
		alert('Something went wrong - check the console');
	}
};


//----
/// @fn SelectItem(element)
/// @brief figures out what needs to be selected between an activity or reminder
//----
timeBudgeter.SelectItem = (element) => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('selecting an item');
	if (tb.logLevel <= 2) console.log('element: ', element);

	try {
		let pNode = element.parentNode;
		let iNode = pNode.querySelector('i');
		let nNode = pNode.querySelector('div.note');
		
		switch(pNode.getAttribute('data-type')) {
			case 'reminder':
				tb.SelectReminder(pNode.id);
			break;
			case 'activity':
			default:
				tb.SelectActivity(pNode.id);
			break;
		}
		
		// toggle note display
		if (element.nodeName === 'I') {
			// iNode.classList.toggle('rot90');
			// nNode.classList.toggle('hidden');
			
			tb.dialog.querySelector('#d_title').innerHTML = pNode.querySelector('span.title').innerHTML;
			tb.dialog.querySelector('#d_note').innerHTML = nNode.innerHTML;
			tb.dialog.showModal();
		}
		
	} catch (ex) {
		console.error(ex);
		alert('Something went wrong - check the console');
	}
};


//==--
// activity management
//==--

//----
/// @fn NewActivity()
/// @brief clears the activity form, enables the add button
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.NewActivity = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('new activity');

	// reset activity fields
	document.querySelector('#a_id').value = 0;
	document.querySelector('#a_type').value = 'activity';
	document.querySelector('#a_title').value = null;
	document.querySelector('#a_note').value = null;
	document.querySelector('#a_importance').selectedIndex = 1;
	document.querySelector('#a_urgence').selectedIndex = 1;
	document.querySelector('#a_due').value = null;
	document.querySelector('#a_due_time').value = null;
	document.querySelector('#a_done').checked = false;

	// enable add and disable update
	document.querySelector('#addActivity').removeAttribute('disabled');
	document.querySelector('#updateActivity').setAttribute('disabled', true);
};


//----
/// @fn AddActivity()
/// @brief adds a new activity record to the data
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.AddActivity = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('add activity');

	let newId = localStorage.getItem('nextId');
	let dueDate = new Date(
		document.querySelector('#a_due').value + 'T' +
		document.querySelector('#a_due_time').value +
		tb.CalculateOffset()
	);
	let newItem = {
		'id': newId,
		'type': document.querySelector('#a_type').value,
		'title': document.querySelector('#a_title').value,
		'note': document.querySelector('#a_note').value,
		'importance': document.querySelector('#a_importance').value,
		'urgence': document.querySelector('#a_urgence').value,
		'due': document.querySelector('#a_due').value +'T'
			+ document.querySelector('#a_due_time').value
			+ tb.CalculateOffset(),
		'done': document.querySelector('#a_done').checked
	};
	tb.data.push(newItem);

	// increment next id
	localStorage.setItem('nextId', ++newId);

	// update the UI
	// tb.ProcessData(tb.data);
	tb.Refresh();
};


//----
/// @fn SelectActivity()
/// @brief sets the activity form, enables the update button, disables add
/// @author Trevor Ratliff
/// @date 2023-03-10
/// @return null
//----
timeBudgeter.SelectActivity = (id) => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('select activity');

	// get item from tb.data
	let item = tb.data.filter(i => i.id == id)[0];
	let due = new Date(item.due);

	// switch tabs
	document.querySelector('#tab_activity').checked = true;

	// reset activity fields
	document.querySelector('#a_id').value = item.id;
	document.querySelector('#a_type').value = item.type;
	document.querySelector('#a_title').value = item.title;
	document.querySelector('#a_note').value = item.note;
	document.querySelector('#a_importance').value = item.importance;
	document.querySelector('#a_urgence').value = item.urgence;
	document.querySelector('#a_due').value = tb.LocalIsoString(due, 'date');
	document.querySelector('#a_due_time').value = tb.LocalIsoString(due, 'time');
	document.querySelector('#a_done').checked = item.done;

	// enable add and disable update
	document.querySelector('#addActivity').setAttribute('disabled', true);
	document.querySelector('#updateActivity').removeAttribute('disabled');
};


//----
/// @fn UpdateActivity()
/// @brief updates the selected activity
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.UpdateActivity = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('update activity');

	// get item from tb.data
	let id = document.querySelector('#a_id').value;
	let item = tb.data.filter(i => i.id == id)[0];

	// set item data to values from form
	item.type = document.querySelector('#a_type').value;
	item.title = document.querySelector('#a_title').value;
	item.note = document.querySelector('#a_note').value;
	item.importance = document.querySelector('#a_importance').selectedOptions[0].value;
	item.urgence = document.querySelector('#a_urgence').selectedOptions[0].value;
	item.due = document.querySelector('#a_due').value + 'T' 
		+ document.querySelector('#a_due_time').value
		+ tb.CalculateOffset();
	item.done = document.querySelector('#a_done').checked;

	// update the UI
	// tb.ProcessData(tb.data);
	tb.Refresh();
};


//==--
// reminder management
//==--

//----
/// @fn NewReminder()
/// @brief clears the reminder form, enables the add button
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.NewReminder = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('new reminder');

	// reset activity fields
	document.querySelector('#r_id').value = 0;
	document.querySelector('#r_type').value = 'reminder';
	document.querySelector('#r_title').value = null;
	document.querySelector('#r_note').value = null;
	document.querySelector('#r_importance').selectedIndex = 1;
	document.querySelector('#r_urgence').selectedIndex = 1;
	document.querySelector('#r_due').value = null;
	document.querySelector('#r_due_time').value = null;
	document.querySelector('#r_schedule').value = null;
	document.querySelector('#r_done').checked = false;

	// enable add and disable update
	document.querySelector('#addReminder').removeAttribute('disabled');
	document.querySelector('#updateReminder').setAttribute('disabled', true);
};


//----
/// @fn AddReminder()
/// @brief Adds a new reminder to the data set
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.AddReminder = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('add reminder');

	let newId = localStorage.getItem('nextId');
	let newItem = {
		'id': newId,
		'type': document.querySelector('#r_type').value,
		'title': document.querySelector('#r_title').value,
		'note': document.querySelector('#r_note').value,
		'importance': document.querySelector('#r_importance').value,
		'urgence': document.querySelector('#r_urgence').value,
		'due': document.querySelector('#r_due').value +'T'
			+ document.querySelector('#r_due_time').value
			+ tb.CalculateOffset(),
		'schedule': document.querySelector('#r_schedule').value,
		'done': document.querySelector('#r_done').checked
	};
	tb.data.push(newItem);

	// increment next id
	localStorage.setItem('nextId', ++newId);

	// update the UI
	// tb.ProcessData(tb.data);
	tb.Refresh();
};


//----
/// @fn SelectReminder()
/// @brief sets the reminder form, enables the update button, disables add
/// @author Trevor Ratliff
/// @date 2023-03-13
/// @return null
//----
timeBudgeter.SelectReminder = (id) => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('select reminder');

	// get item from tb.data
	let item = tb.data.filter(i => i.id == id)[0];
	let due = new Date(item.due);
	
	// switch tabs
	document.querySelector('#tab_reminder').checked = true;

	// reset activity fields
	document.querySelector('#r_id').value = item.id;
	document.querySelector('#r_type').value = item.type;
	document.querySelector('#r_title').value = item.title;
	document.querySelector('#r_note').value = item.note;
	document.querySelector('#r_importance').value = item.importance;
	document.querySelector('#r_urgence').value = item.urgence;
	document.querySelector('#r_due').value = tb.LocalIsoString(due, 'date');
	document.querySelector('#r_due_time').value = tb.LocalIsoString(due, 'time');
	document.querySelector('#r_schedule').value = item.schedule;
	document.querySelector('#r_done').checked = item.done;

	// enable add and disable update
	document.querySelector('#addReminder').setAttribute('disabled', true);
	document.querySelector('#updateReminder').removeAttribute('disabled');
};


//----
/// @fn UpdateReminder()
/// @brief updates the selected reminder
/// @author Trevor Ratliff
/// @date 2023-02-10
/// @return null
//----
timeBudgeter.UpdateReminder = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('update reminder');

	// get item from tb.data
	let id = document.querySelector('#r_id').value;
	let item = tb.data.filter(i => i.id == id)[0];

	// set item data to values from form
	item.type = document.querySelector('#r_type').value;
	item.title = document.querySelector('#r_title').value;
	item.note = document.querySelector('#r_note').value;
	item.importance = document.querySelector('#r_importance').selectedOptions[0].value;
	item.urgence = document.querySelector('#r_urgence').selectedOptions[0].value;
	item.due = document.querySelector('#r_due').value + 'T' 
		+ document.querySelector('#r_due_time').value
		+ tb.CalculateOffset();
	item.schedule = document.querySelector('#r_schedule').value;
	item.done = document.querySelector('#r_done').checked;

	// update the UI
	// tb.ProcessData(tb.data);
	tb.Refresh();

};


//==--
// import and export
//==--

//----
///	@fn ImportData()
///	@brief brings in and parses data from the textarea, saves to storage
///	@author Trevor Ratliff
///	@date 2023-02-09
///	@return
//----
timeBudgeter.ImportData = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('importing data');

	try {
		// parse data
		tb.data = JSON.parse(tb.exportElement.value);
		/*// tb.data.sort(tb.SortItems);
		tb.data.sort(common.sortItems);
		tb.ProcessData(tb.data);*/

		// store data
		tb.dataString = JSON.stringify(tb.data);
		localStorage.setItem('TimeBudgeter', tb.dataString);

		// send data to scheduler
		tb.UpdateScheduler();
	} catch (ex) {
		console.error(ex);
		alert('something went wrong - check the console');
	}
};


//----
///	@fn ExportData()
///	@brief stringifies TimeBudgeter.data, and puts the string in the textarea
///	@outhor Trevor Ratliff
///	@date 2023-02-09
///	@return
//----
timeBudgeter.ExportData = () => {
	let tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('exporting data');
	try {
		tb.dataString = JSON.stringify(tb.data);
		tb.exportElement.value = tb.dataString;
	} catch (ex) {
		console.error(ex);
		alert('something went wrong - check the console');
	}
};


//----
/// @fn Refresh()
/// @brief refreshes the data and screen
//----
timeBudgeter.Refresh = () => {
	let tb = timeBudgeter || {};
	if (tb.logLevel <= 5) console.log('Refreshing and resyncing data');
	
	if (tb.logLevel <= 1) console.log('Refresh: Process Data');
	if (tb.logLevel <= 2) console.log('Refresh: preprocess Data: ', tb.data);
	tb.ProcessData(tb.data);
	if (tb.logLevel <= 2) console.log('Refresh: postprocess Data: ', tb.data);
	
	if (tb.logLevel <= 1) console.log('Refresh: Sort Data');
	tb.data.sort(common.sortItems);
	if (tb.logLevel <= 2) console.log('Refresh: Sorted Data: ', tb.data);
	
	if (tb.logLevel <= 1) console.log('Refresh: Export Data');
	tb.ExportData();
	if (tb.logLevel <= 2) console.log('Refresh: Exported Data: ', tb.data);
	
	if (tb.logLevel <= 1) console.log('Refresh: Import Data');
	tb.ImportData();
	if (tb.logLevel <= 2) console.log('Refresh: Imported Data: ', tb.data);
};


//----
/// @fn ManageNotify()
/// @brief manages if notification api can be used
//----
timeBudgeter.ManageNotify = () => {
	// test if notifications have not been allowed yet
	if (!!Notification && !!Notification.permission) {
		if (Notification.permission !== 'granted') {
			// request permission
			Notification.requestPermission().then((result) => {
				if (tb.LogLevel <= 5) console.log(result);
			});
		}
	}
};


//==--
// helpers
//==--

//----
/// @fn CalculateGridSection()
/// @brief Determins the correct grid for the passed in item
/// @author Trevor Ratliff
/// @date 2023-02-09
/// @return
//----
timeBudgeter.CalculateGridSection = (item) => {
	// ToDo: determin if this needs to signal when a change in grid happens so data can be updated correctly???
	let tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('picking the correct ui grid');

	try {
		let isImportant = item.importance.toLowerCase() === 'important';
		let isUrgent = item.urgence.toLowerCase() === 'urgent';
		let isActivity = item.type === 'activity';
		let due = Date.parse(item.due);
		let now = Date.now();
		let diffMilli = (due - now);
		let diffConvert = 1;

		// if important and urgent or past due return early
		if (isImportant && isUrgent || due < now) 
		{
			// set item's state
			item.importance = 'important';
			item.urgence = 'urgent';

			return 'important_urgent';
		}

		// determin if the due date affects importance
		if (isActivity) {
			diffConvert = tb.CalculateConverter(tb.activityImportanceScale);
			if ((diffMilli/diffConvert) < tb.activityImportanceValue)
				isImportant = true;
		} else {
			diffConvert = tb.CalculateConverter(tb.reminderImportanceScale);
			if ((diffMilli/diffConvert) < tb.reminderImportanceValue)
				isImportant = true;
		}

		// determin if the due date affects urgency
		if (isActivity) {
			diffConvert = tb.CalculateConverter(tb.activityUrgencyScale);
			if ((diffMilli/diffConvert) < tb.activityUrgencyValue)
				isUrgent = true;
		} else {
			diffConvert = tb.CalculateConverter(tb.reminderUrgencyScale);
			if ((diffMilli/diffConvert) < tb.reminderUrgencyValue)
				isUrgent = true;
		}

		// set item's state
		item.importance = isImportant ? 'important' : 'unimportant';
		item.urgence = isUrgent ? 'urgent' : 'nonurgent';

		// return the correct grid identifier
		if (isImportant && isUrgent) return 'important_urgent';
		if (isImportant && !isUrgent) return 'important_nonurgent';
		if (!isImportant && isUrgent) return 'unimportant_urgent';

	} catch (ex) {
		console.error(ex);
		alert('something went wrong - check the console');
	}

	return 'unimportant_nonurgent';
};


//----
/// @fn CalculateConverter()
/// @brief calculates the multiplying factor for time difference calculations
//----
timeBudgeter.CalculateConverter = (value) => {
	let diffConvert = 1;
	switch(value) {
		case 'days':
			diffConvert = 1000*60*60*24;
		case 'hours':
			diffConvert = 1000*60*60;
			break;
		case 'minutes':
			diffConvert = 1000*60;
			break;
		default:
			diffConvert = 1000;
	}
	return diffConvert;
};


//----
/// @fn CalculateOffset()
/// @brief generates the timezone offset for date manipulations
//----
timeBudgeter.CalculateOffset = (date) => {
	const tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('calculating user\'s time offset');

	const padLength = 2;
	const tmpDate = date || new Date();
	const localOffset = tmpDate.getTimezoneOffset();
	const hourOffset = Math.floor(localOffset / 60);
	const minOffset = (localOffset) % 60;
	const offset = (localOffset > 0 ? '-' : '+')
		+ `${hourOffset}`.padStart(padLength, '0')
		+ `${minOffset}`.padStart(padLength, '0');

	return offset;
};


//----
/// @fn LocalIsoString()
/// @brief converts a passed in date to an ISO string with timezone offset
//----
timeBudgeter.LocalIsoString = (date, part) => {
	const tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('generating local ISO date string');
	
	const ret = date.getFullYear()
		+ '-' + tb.Pad((date.getMonth() + 1), 2, '0', 'left')
		+ '-' + tb.Pad(date.getDate(), 2, '0', 'left')
		+ 'T' + tb.Pad(date.getHours(), 2, '0', 'left')
		+ ':' + tb.Pad(date.getMinutes(), 2, '0', 'left')
		+ ':' + tb.Pad(date.getSeconds(), 2, '0', 'left')
		+ tb.CalculateOffset(date);
	
	if (part === 'date') return ret.substring(0,10);
	if (part === 'time') return ret.substring(11, 19);

	return ret;
};


//----
/// @fn NotifyUser()
/// @brief raises a notification based on user options
//----
timeBudgeter.NotifyUser = (message) => {
	let tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('notifying the user');
	
	// test if notifications are on
	if (tb.noticeOn) {
		if (Notification.permission === 'granted') {
			// use system notify to send message
			const notice = new Notification('TimeBudgeter: Notice', { 'body': message, 'icon': '' });
		} else {
			// use alert dialog -> eventually this will be a pop-over instead
			alert(message);
		}
	} else {
		// log message
		if (tb.LogLevel <= 5) console.log(message);
	}
};


//----
/// @fn Pad()
/// @brief takes an input and ensures it meets a required length padding with the passed in value
//----
timeBudgeter.Pad = (value, length, padChar, direction) => {
	let ret = value.toString();

	if (direction.toLowerCase() === 'right') {
		ret = ret.padEnd(length, padChar);
	} else {
		ret = ret.padStart(length, padChar);
	}

	return ret;
};


//----
/// @fn UpdateScheduler()
/// @brief sends updated data to the scheduler
//----
timeBudgeter.UpdateScheduler = () => {
	let tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('updating the scheduler');

	// update scheduler
	if (!!tb.scheduler.postMessage) {
		tb.scheduler.postMessage({ 
			'type': 'dataLoad'
			, 'meta': { 
				'intervals': { 
					'default': tb.CalculateConverter('minutes') * tb.interval
					, 'notice': tb.CalculateConverter('minutes') * tb.noticeInterval 
				} 
			}
			, 'message': tb.data 
		});
	}
};


//----
/// @fn timeBudgeter.PassMethods()
/// @brief pass shared methods to the worker - probably aught to use module for shared methods
//----
timeBudgeter.PassMethods = () => {
	let tb = timeBudgeter || {};
	if (tb.logLevel <= 2) console.log('passing shared methods');

	// update scheduler
	if (!!tb.scheduler.postMessage) {
		tb.scheduler.postMessage({ 
			'type': 'passMethods'
			, 'methods': { 
				'sort': 'tb.SortItems doesn\'t work so we need to import a mobule of common functions' 
			}
			, 'message': 'passing in the following methods: [sort]'
		});
	}
};


//export { timeBudgeter };

//==--
// page functions
//==--

//----
// imediate function to initialize application
//----
(function(){ 
	let tb = timeBudgeter || {};
	tb.Init();
})();


//----
// page loaded event to initialize the UI
//----
window.onload = (e) => {
	let tb = timeBudgeter || {};
	console.log('TimeBudgeter has finished loading');
	if (tb.logLevel <= 2) console.log(e);

	// set ui grid refs
	tb.importantUrgent = document.querySelector('#important_urgent');
	tb.importantNonurgent = document.querySelector('#important_nonurgent');
	tb.unimportantUrgent = document.querySelector('#unimportant_urgent');
	tb.unimportantNonurgent = document.querySelector('#unimportant_nonurgent');

	// set export info
	tb.exportElement = document.querySelector('#manage_data');
	tb.exportElement.value = tb.dataString;

	// set template refs
	tb.itemTemplate = document.querySelector('#data_template');
	tb.dialog = document.querySelector('#alerts');

	// set data
	tb.ProcessData(tb.data);

	// send data to scheduler
	tb.UpdateScheduler();

	// send sort method to scheduler
	tb.PassMethods();
};


//----
// test to see if workors can be used
//----
if (window.location.host != '' || window.location.origin != 'file://') {
	timeBudgeter.scheduler = new Worker(
		'./scripts/scheduler.js?v=1'
		+ `&interval=${timeBudgeter.CalculateConverter('minutes') * timeBudgeter.interval}`
		+ `&notice=${timeBudgeter.CalculateConverter('minutes') * timeBudgeter.noticeInterval}`
		+ `&logLevel=${timeBudgeter.logLevel}`
	);

	timeBudgeter.scheduler.onmessage = (e) => {
		let tb = timeBudgeter || {};
		if (tb.LogLevel <= 5) console.log('message received');
		if (tb.logLevel <= 2) console.log('    type:', e.data.type);
		
		switch (e.data.type) {
			case 'processData':
				//tb.ProcessData(tb.data);
				tb.Refresh();
				break;
			case 'notice':
				tb.NotifyUser(e.data.message);
				break;
			default:
				if (tb.LogLevel <= 5) console.log(e.data.message);
				break;
		};
	};
} else {
	timeBudgeter.scheduler = window.setInterval(() => {
		timeBudgeter.Refresh();
	}, timeBudgeter.CalculateConverter('minutes') * timeBudgeter.interval);

	timeBudgeter.notice = window.setInterval(() => {
		alert('What are you supposed to be working on?');
	}, timeBudgeter.CalculateConverter('minutes') * timeBudgeter.noticeInterval);
}

