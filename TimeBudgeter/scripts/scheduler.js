//====
/// @file scheduler.js
/// @brief This file defines a web worker scirpt to controll the timing and 
///	notifications
/// @author Trevor Ratliff
/// @date 2023-03-13
//  
//  Definitions:
//	timeBudgeter - object holding main application logic
//	console - object to ensure logging can happen
//	common - variable access for the imported common code
//
/// @verbatim
/// History:  Date  |  Programmer  |  Contact  |  Description  |
///	2022-03-13  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  file 
///		creation in order to try out a worker script and to help manage timers
///		and notifications to the main timeBudgeter script via messages  |
///	2023-04-04  |  Trevor Ratliff  |  trevor.w.ratliff@gmail.com  |  started 
///		introducing a module for common code - like the sort  |
/// @endverbatim
//====

var timeBudgeter = timeBudgeter || {
	'current': null,
	'data': null,
	'logLevel': 6		// 'trace' = 1, 'debug' = 2, 'warn' = 3, 'error' = 4, 'info' = 5
};

var console = console ?? 
{ 
	'log': (data) => { alert(data); }, 
	'error': (data) => { alert(data); } 
};


//----
// imprort sort function
//----
var common = {};
import("./common.js?v=0.3").then((module) => {
// Do something with the module.
	common = module;
	if (tb.LogLevel <= 2) console.log('scheduler: loaded common module', common);
});


//----
/// @fn timeBudgeter.GenerateMessage()
/// @brief generates the notice message
//----
timeBudgeter.GenerateMessage = () => {
	let tb = timeBudgeter || {};
	let messagePart = 'you are working on';
	
	if (tb.LogLevel <= 5) console.log('scheduler: generating the notice message');
	if (tb.logLevel <= 2) console.log(`    from item: [${tb.current.title}]`);

	// see if the current item is past due and notify appropriately
	if (tb.current.isDue) messagePart = 'the following item is due';
	
	return `remember ${messagePart} \r\n[${tb.current.title}]`;
};


//----
/// @fn timeBudgeter.SetCurrentItem()
/// @brief this will figure out what the most urgent/important item is and set it in timeBudgeter.current
//----
timeBudgeter.SetCurrentItem = () => {
	tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('scheduler: setting the current item');

	// sort items then filter out items that are done
	// tb.data.sort(tb.SortItems);
	const dataFiltered = tb.data
		.filter((item) => { return !item.done; })
		.sort(common.sortItems);
	if (tb.logLevel <= 2) console.log('scheduler: sorted filtered data', dataFiltered);

	tb.current = dataFiltered[0];
	tb.current.isDue = false;
	if (Date.parse(tb.current.due) <= new Date()) tb.current.isDue = true;
	if (tb.logLevel <= 2) console.log('scheduler: found', timeBudgeter.current);
};


//----
/// @fn self.SchedulerInit()
/// @brief initializes the schedule web worker
//----
self.SchedulerInit = () => {
	tb = timeBudgeter || {};
	console.log('scheduler: Initialized the TimeBudgeter scheduling web worker');

	tb.query = self.location.search.match(/([^?&=]+)/gi);
	tb.q = {};
	for (let ii = 0; ii < tb.query.length; ii+=2) {
		tb.q[tb.query[ii]] = tb.query[ii + 1];
	}

	// set log level: 'trace' = 1, 'debug' = 2, 'warn' = 3, 'error' = 4, 'info' = 5
	tb.logLevel =
		parseInt(tb.q.logLevel) ||
		parseInt(localStorage.getItem('logLevel')) ||
		tb.logLevel;
	if (isNaN(tb.logLevel)) tb.logLevel = 5;

	// set schedules for processData and notice messages
	tb.schedules = [
		{ 
			'id': 'interval'
			, 'schedule': self.setInterval(
				() => { 
					tb.SetCurrentItem();
					self.postMessage({
					'type': 'processData'
					, 'message': 'time to process data'
					});
					if (tb.current.isDue) {
						self.postMessage({
							'type': 'notice'
							, 'message': tb.GenerateMessage()
						});
					}
				}
				, tb.q.interval
			)
		}
		, {
			'id': 'notice'
			, 'schedule': self.setInterval(
				() => { self.postMessage({
					'type': 'notice'
					, 'message': tb.GenerateMessage()
				}); }
				, tb.q.notice
			)
		}
	];
};


//----
/// @fn self.onmessage()
/// @brief handles incoming messages, calling the appropriate methods
//----
self.onmessage = (e) => {
	tb = timeBudgeter || {};
	if (tb.LogLevel <= 5) console.log('scheduler: message received');
	if (tb.logLevel <= 2) console.log('    message type', e.data.type);
	
	switch (e.data.type) {
		case 'dataLoad':
			tb.data = e.data.message;
			self.postMessage({ 'type': 'info', 'message': 'scheduler: data loaded' });
			break;

		case 'passMethods':
			// sort
			tb.SortItemsFn = e.data.methods.sort;

			self.postMessage({ 'type': 'info', 'message': 'scheduler: sort method received' });
			break;

		default:
			if (tb.LogLevel <= 5) console.log('scheduler: ', e.data.message);
			break;
	}
};


//----
/// @fn ()
/// @brief imediately invoked function that starts initialization
//----
(function (){
	console.log('scheduler: running the TimeBudgeter web worker');
	self.SchedulerInit();
})();
